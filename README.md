# LaTeX-makefile

An overkill Makefile for compiling LaTeX documents.
It uses md5sums to determine whether rebuilds are necessary instead of the usual timestamp.
It is extensible enough to replace Latexmk's "custom dependency" system.

by twl <i3wgnit@protonmail.com>

## Motivation

### Now, "why would you ever use such an overkill Makefile just to not write custom dependencies for Latexmk? Are you allergic to perl?", you ask.

Yes I am, but also, Latexmk does not cleanup well enough in my experience, it ends up leaving `.asy` files here and `.dot` files there.
I don't like that, hence I wrote this Makefile to get rid of them with a simple `make cleandeps`.

### "Oh, but Latexmk has this `@generated_ext` feature. How about you use that instead of writing an overkill Makefile?"

I use subdirectories for generating asymptote and graphviz files, and Latexmk does not remove these files from subdirectories as far as I know.
If you know how to do this, please contact me and I will deprecate this makefile.

## Dependencies

- GNUMake (GNU implementation is mainly for `:=`, pattern matching targets, and GNU's set of automatic variables)
- Latexmk
- md5sum
- sed and awk (I am pretty sure I only used POSIX features)

## Usage

Modify the TARGETS variable in the Makefile accordingly (defaults to main.pdf)

The following targets are available
- all (default)
  - This is equivalent to running pdf, dvi, and ps.
- pdf (pdflatex)
- pdflua (lualatex)
- pdfxe (xelatex)
- dvi (latex)
- ps
- clean (clean everything, including TARGETS)
- cleandeps (keep TARGETS)
