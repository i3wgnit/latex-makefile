# ===
# An overkill Makefile for compiling LaTeX documents
# by twl <i3wgnit@protonmail.com>
# ===

TARGETS := main.pdf

# ===

all: options pdfoptions dvioptions psoptions dirs $(TARGETS)

# ===
# Customizable Variables
# ===

PDFTGT := $(filter %.pdf,$(TARGETS))
DVITGT := $(filter %.dvi,$(TARGETS))
PSTGT := $(filter %.ps,$(TARGETS))

pdf: options pdfoptions dirs $(PDFTGT)
dvi: options dvioptions dirs $(DVITGT)
ps: options psoptions dirs $(PSTGT)

pdflua: pdf
pdfxe: pdf

LATEXMK := latexmk
LATEXMKFLAGS := -interaction=nonstopmode -cd
LATEXMKBUILDFLAGS := -recorder -use-make --synctex=1 -f -M -MP \
	-e 'warn qq(In Makefile, turn off custom dependencies\n);' \
	-e '@cus_dep_list = ();' \
	-e 'show_cus_dep();'

LATEXMKPDFFLAGS := -pdflatex
LATEXMKDVIFLAGS := -latex
LATEXMKPSFLAGS := -pdf- -dvi- -ps
pdflua: LATEXMKPDFFLAGS := -lualatex
pdfxe: LATEXMKPDFFLAGS := -xelatex

XINDY := xindy
XINDYFLAGS := -L english -C utf8 -I xindy

# ===
# Customizable Flags
# ===

FALSY := 0 off OFF false FALSE
TRUTHY := 1 on ON true TRUE

ifdef WATCH
ifneq (,$(findstring $(WATCH),$(TRUTHY)))
LATEXMKBUILDFLAGS += -pvc

# Force build TARGETS
$(TARGETS): FORCE_MAKE
endif
endif

ifdef VIEW
ifneq (,$(findstring $(VIEW),$(FALSY)))
LATEXMKBUILDFLAGS += -view=none
endif
endif

ifdef QUIET
ifneq (,$(findstring $(QUIET),$(TRUTHY)))
LATEXMKBUILDFLAGS += -quiet
endif
endif

# ===
# Nothing to see here
# ===

BADTARGETS := $(filter-out $(addprefix $(realpath .)/,$(notdir $(TARGETS))),$(realpath $(TARGETS)))
ifneq (,$(BADTARGETS))
$(warning The following TARGETS are causing errors:)
$(warning $(BADTARGETS))
$(error This Makefile can only produce targets in the same directory as it)
endif

# ===

options:
	@echo build options:
	@echo "LATEXMK           = $(LATEXMK)"
	@echo "LATEXMKFLAGS      = $(LATEXMKFLAGS)"
	@echo "LATEXMKBUILDFLAGS = $(LATEXMKBUILDFLAGS)"
pdfoptions:
	@echo "LATEXMKPDFFLAGS   = $(LATEXMKPDFFLAGS)"
dvioptions:
	@echo "LATEXMKDVIFLAGS   = $(LATEXMKDVIFLAGS)"
psoptions:
	@echo "LATEXMKPSFLAGS    = $(LATEXMKPSFLAGS)"

# ===
# MD5 hack
# ===

MD5S :=
to-md5 = $(addsuffix .md5,$(1:.md5=))
from-md5 = $(1:.md5=)
%.md5: %
	@CHECKSUM="$$(md5sum $<)"; \
	if ! { [ -r $@ ] && [ "$$(cat $@)" = "$$CHECKSUM" ] ; }; then \
		echo "$$CHECKSUM" > $@; \
	fi

# Keep make from removing .md5 files
.PRECIOUS: %.md5

# ===
# Generic build rules for .tex files
# ===

# This is an ugly hack to change the automatically generated dependencies to .md5 for relative paths
ugly-md5-hack = TMP="$$(mktemp)" && \
	trap 'rm -f "$$TMP"' EXIT && \
	awk 'm = !/^[[:space:]]*(\#|\/|$$)|(\.tex|:)[[:space:]]*(\\|)$$/ {print "-" $$0} !m {print "_" $$0}' $(1).d |\
		sed 's/^-\(.*[^\\]\)\(\\\|\)$$/_\1.md5\2/ ; s/^_//' > "$$TMP" && \
	mv "$$TMP" $(1).d

%.pdf: %.tex
	$(LATEXMK) $(LATEXMKFLAGS) $(LATEXMKBUILDFLAGS) -MF $<.d $(LATEXMKPDFFLAGS) $<
	@$(call ugly-md5-hack,$<)

%.dvi: %.tex
	$(LATEXMK) $(LATEXMKFLAGS) $(LATEXMKBUILDFLAGS) -MF $<.d $(LATEXMKDVIFLAGS) $<
	@$(call ugly-md5-hack,$<)

%.ps: %.tex
	$(LATEXMK) $(LATEXMKFLAGS) $(LATEXMKBUILDFLAGS) -MF $<.d $(LATEXMKPSFLAGS) $<
	@$(call ugly-md5-hack,$<)

# ===
# Custom Dependencies
# ===

PDFSRC := $(PDFTGT:.pdf=.tex)
DVISRC := $(DVITGT:.dvi=.tex)
PSSRC := $(PSTGT:.ps=.tex)
SRC := $(sort $(PDFSRC) $(DVISRC) $(PSSRC))
DEP := $(addsuffix .d,$(SRC))
DIR := $(sort $(dir $(SRC)))

-include $(DEP)

add-prefixes = $(foreach PREFIX,$(1),$(addprefix $(PREFIX),$(2)))
add-suffixes = $(foreach SUFFIX,$(1),$(addsuffix $(SUFFIX),$(2)))
add-presuffixes = $(call add-prefixes,$(1),$(call add-suffixes,$(2),$(3)))

OBJ :=
OBJDIR :=

# glossaries-extra
do-gls = $(XINDY) $(XINDYFLAGS) -M $(2) -t $(2).glg$(3) -o $(1) $(2).glo$(3)
%.gls: $(call to-md5,%.glo)
	$(call do-gls,$@,$*,)
%.gls-abr: $(call to-md5,%.glo-abr)
	$(call do-gls,$@,$*,-abr)
OBJ += $(wildcard $(call add-suffixes,.gls .glo .gls-abr .glo-abr,$(SRC:.tex=)))
# Some other generated files we didn't really expect, but still got
OBJ += $(wildcard $(call add-suffixes,.glg .glg-abr,$(SRC:.tex=)))

# asymptote
%.eps %.pdf %.tex: $(call to-md5,%.asy)
	asy -o $* $*
OBJ += $(wildcard $(addprefix asy/*,.asy .eps .pdf .tex))
OBJ += $(wildcard $(addprefix asy/,*.pre $(call add-suffixes,.log .aux,$(notdir $(SRC:.tex=)))))
OBJDIR += asy

# my fork of dot2texi
do-dot2tex = dot2tex --prog=$(subst .,,$(suffix $(2))) --autosize --figonly --nominsize --texmode math -o $(1) $(2)
%.tex: $(call to-md5,%.dot)
	$(call do-dot2tex,$@,$(call from-md5,$<))
%.tex: $(call to-md5,%.dot.neato)
	$(call do-dot2tex,$@,$(call from-md5,$<))
%.tex: $(call to-md5,%.dot.circo)
	$(call do-dot2tex,$@,$(call from-md5,$<))
%.tex: $(call to-md5,%.dot.twopi)
	$(call do-dot2tex,$@,$(call from-md5,$<))
OBJ += $(wildcard $(addprefix gv/*,.dot .dot.neato .dot.circo .dot.twopi .tex))
OBJDIR += gv

$(DIR) $(OBJDIR):
	mkdir $@
dirs: $(DIR) $(OBJDIR)


MD5S += $(call to-md5,$(OBJ))

# ===
# Cleanups
# ===

# Filter out non-existant FLS
cleandeps: FLS := $(wildcard $(sort $(SRC:.tex=.fls)))
# Parse FLS for output files
cleandeps: OUTFILES := $(if $(FLS),$(filter-out $(TARGETS),$(sort $(shell sed -n '/^O/s/^OUTPUT //p' $(FLS)))))
# Parse FLS for `.tex` input files that have a relative path
cleandeps: INTEXFILES := $(if $(FLS),$(filter-out $(SRC),$(sort $(shell sed -n '/^INPUT [^/].*\.tex$$/s/^INPUT //p' $(FLS)))))

cleandeps:
	$(LATEXMK) $(LATEXMKFLAGS) -f -quiet -c $(SRC) $(INTEXFILES)
	rm -f $(DEP) $(OUTFILES) $(OBJ) $(MD5S)
	rm -rf $(OBJDIR)

clean: cleandeps
	$(LATEXMK) $(LATEXMKFLAGS) -f -quiet -C $(SRC)
	rm -f $(TARGETS)

# ===

.PHONY: all pdf dvi ps pdflua pdfxe dirs options pdfoptions dvioptions psoptions cleandeps clean
.PHONY: FORCE_MAKE
